# semgrep-ignore-0.69.1

This project contains two 2 python files where one has an inline comment used to allow semgrep to ignore findings, while the other does not.
This project is tested using the latest analyzer that uses semgrep version 0.69.1
